package eu.dc.magicdao;

import eu.dc.magicdao.interfaces.DAOOperator;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by dc on 07.12.2015.
 */
public class DAO {

    public static ConcurrentHashMap<Class, DAOEntity> registeredClasses = new ConcurrentHashMap<>();
    public static DAOOperator operator;

    public static void init(DAOOperator op) {
        operator=op;
        Morphium.defaultMorphes();
    }

    public static DAOOperator getOperator() {
        return operator;
    }

    public static void setOperator(DAOOperator operator) {DAO.operator = operator;}

    public static boolean insert(Object target) {return operator.insert(target);}

    public static Object load(Object target) { return operator.load(target);}

    public static boolean update(Object target) { return operator.update(target);}

    public static Object findby(Object target, Field... fieldie) {return operator.findby(target, fieldie);}

    public static List<String> findAll(Class c) {return operator.findAll(c);}

    public static boolean prePareForAction(Class c){
        if(DAO.registeredClasses.containsKey(c))
            return true;
        else
        {
            boolean suceeded = DomainClassHandler.registerMagicDaoClass(c);
            EventHandler.fireEventNewDAOEntityEvent(DAO.registeredClasses.get(c));
            return suceeded;
        }
    }
}