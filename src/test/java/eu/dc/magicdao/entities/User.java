package eu.dc.magicdao.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Join;
import eu.dc.magicdao.annotations.Table;
import eu.dc.magicdao.annotations.loadoption.Load;
import eu.dc.magicdao.util.enums.Relation;

import java.util.Date;

@Table(tablename = "U_USERS")
public class User {
    @Column(fieldname = "u_id", primarykey = true)
    public int id;
    @Column(fieldname = "u_username")
    public String username;
    @Column(fieldname = "u_password")
    public String password;
    @Column(fieldname = "u_favday")
	public Date favDay;

    public User(String username, String password, Date favDay) {
        this.username = username;
        this.password = password;
		this.favDay = favDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}