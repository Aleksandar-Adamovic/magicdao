package eu.dc.magicdao.operator.defaults;

import eu.dc.magicdao.DAO;
import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by dc on 11.04.2016.
 */
public class SQLPreparedStatements {

    public PreparedStatement insert;
    public PreparedStatement load;
    public PreparedStatement update;
    public PreparedStatement findby;//TODO
    public PreparedStatement findall;//TODO

    public SQLOperations executer;

    DAOEntity entity;
    public SQLPreparedStatements(DAOEntity entity, SQLOperations executer){
        this.entity=entity;
        this.executer=executer;
    }

    public void prepareStatements() {
        prepareInsertStatement();
        prepareUpdateStatement();
        prepareLoadStatement();
    }

    private void prepareInsertStatement() {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO " + entity.table_name + " (");
        for (Field f : entity.fields) {
            builder.append(entity.getFieldName(f) + ",");
        }
        builder.deleteCharAt(builder.toString().length() - 1);
        builder.append(") VALUES (");
        for (Field f : entity.fields) {
            f.setAccessible(true);
            builder.append(("?") + ",");
        }
        builder.deleteCharAt(builder.toString().length() - 1);
        builder.append(")");
        Log.write(builder.toString(), LogLevel.DEBUG);
        try {
            insert = executer.connection.prepareStatement(builder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void prepareUpdateStatement() {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE " + entity.table_name);
        builder.append(" SET ");
        for (Field f : entity.fields) {
            f.setAccessible(true);
            builder.append(entity.getFieldName(f) + "= ?,");
        }
        builder.deleteCharAt(builder.toString().length() - 1);
        boolean where = true;
        for (int i = 0; i < entity.fields.size(); i++) {
            try {
                entity.fields.get(i).setAccessible(true);
                if (entity.fields.get(i).getAnnotation(Column.class).primarykey()) {
                    builder.append((where ? " WHERE " : " AND ") +
                            entity.getFieldName(entity.fields.get(i)) + "= " + "?");
                    where = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        builder.append(";");
        Log.write(builder.toString(), LogLevel.DEBUG);

        try {
            update = executer.connection.prepareStatement(builder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void prepareLoadStatement() {
        ArrayList<Field> primary_key = entity.primary_keys;

        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM ").append(entity.table_name);
        builder.append(" WHERE ");
        for (Field f : primary_key) {
            builder.append(f.getAnnotation(Column.class).fieldname());
            builder.append("=");
            f.setAccessible(true);
            builder.append("?");
            builder.append(" AND ");
        }

        String text = builder.substring(0, builder.length() - 4) + ";";
        Log.write(text.toString(), LogLevel.DEBUG);
        try {
            load = executer.connection.prepareStatement(text);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
