package eu.dc.magicdao.interfaces;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by me on 10.04.2016.
 */
public interface DAOOperator {

    boolean insert(Object target);

    Object load(Object target);

    boolean update(Object target);

    Object findby(Object target, Field ... fieldie);

    List<String> findAll(Class c);







}
